from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import Lasso
import pandas as pd
import numpy as np
import xgboost as xgb
import pickle
import os
import string
import bisect
import warnings
import re

class BuildEstimator:

    @staticmethod
    def createBlindTestSamples():
            
        try:
            os.remove('api/lib/data/fitSample.csv')
            os.remove('api/lib/data/blindedTestSample.csv')
        except OSError:
            pass
        #Read the csv
        rawData = pd.read_csv('api/lib/data/vgsales_edited.csv')
        print(rawData.info())
        #Drop columns not needed for building prediction model
        rawData = rawData.drop(columns=['Name','Platform','Year_of_Release','Publisher','Critic_Score','Critic_Count','User_Score','User_Count','Developer','Rating'])
        #Select only the necessary columns
        rawData.columns = ['Genre','NA_Sales','EU_Sales','JP_Sales','Other_Sales','Global_Sales']
        rawData['Genre'] = [str(x) for x in rawData['Genre']]

        #Build label for regression
        labelDict = {}
        labelDict['Genre'] = LabelEncoder()
        rawData['Genre'] = labelDict['Genre'].fit_transform(rawData['Genre'])
        curLbl = labelDict['Genre'].classes_.tolist()
            
        leOutput = os.path.join('api/lib/data/labelDict.pickle')
        file = open(leOutput,'wb')
        pickle.dump(labelDict,file)
        file.close()
        
        X = rawData.drop('Global_Sales', axis=1)
        Y = rawData['Global_Sales']
        XFit, XBindTest, yFit, yBlindTest = train_test_split(X,Y,test_size = 0.3)
        
        column_head = pd.Index(['y']).append(XFit.columns)
        train= pd.DataFrame(np.column_stack([yFit,XFit]),columns=column_head)
        blind=pd.DataFrame(np.column_stack([yBlindTest,XBindTest]),columns=column_head)
        
        train.to_csv('api/lib/data/fitSample.csv', index=False)
        blind.to_csv('api/lib/data/blindedTestSample.csv', index=False)

    @staticmethod
    def getBestPipeline(X,y):

        search_params = {'alpha':[0.01,0.05,0.1,0.5,1]}
        search_params = dict(('estimator__'+k, v) for k, v in search_params.items())

        search_params['normalizer'] = [None,StandardScaler()]
        search_params['featureSelector'] = [None,PCA(n_components=0.90, svd_solver='full')]
            
        pipe = Pipeline(steps=[
            ('normalizer',None),
            ('featureSelector', None),
            ('estimator', Lasso()) 
        ])
         
        cv = GridSearchCV(pipe,search_params,cv=10,verbose=0,scoring='neg_mean_squared_error',n_jobs=-1,error_score=0.0)
        cv.fit(X, y)
            
        return cv

    @staticmethod
    def createModel():

        fit = pd.read_csv('api/lib/data/fitSample.csv')
        XFit = fit.drop(['y'],axis=1)
        yFit = fit['y']
            
        blindTest = pd.read_csv('api/lib/data/blindedTestSample.csv')
        XBlindTest = blindTest.drop(['y'],axis=1)
        yBlindTest = blindTest['y']
            
        optimizedModel = BuildEstimator.getBestPipeline(XFit,yFit).best_estimator_
        yPredFit = optimizedModel.predict(XFit)
        yPredTest = optimizedModel.predict(XBlindTest)
            
        fit_score = mean_squared_error(yFit,yPredFit)
        test_score = mean_squared_error(yBlindTest,yPredTest)
            
        print("fit mse = %.2f and test mse = %.2f" %(fit_score,test_score))
            
        file = open('api/lib/model/vgsales_edited.csv','wb')
        pickle.dump(optimizedModel,file)
        file.close()
        
if __name__ == '__main__':
    BuildEstimator.createBlindTestSamples()
    BuildEstimator.createModel()